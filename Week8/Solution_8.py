#!/usr/bin/env python2

# -*- coding: utf-8 -*-

"""

Created on Sat Dec 12 00:00:00 2021


@author: vitz

"""




from array import array
from numpy import sqrt,exp,pi

import ctypes 
import numpy as np

from ROOT import gROOT,gStyle,TMinuit,TH1F,TF1,TCanvas,TLegend,TLatex,TMarker,TArrow,TGraph

########################################################################

def f1(			x					,\
				par					):

   val		=	0;

   m		=	x[0];
   
   A		=	par[0];
   x0		=	par[1];
   sigma	=	par[2];
   b		=	par[3];
   
   arg		=	( m - x0 ) / sigma;

   val 		= 	A / ( sqrt(2*pi) * sigma ) * exp( -0.5 * arg**2 ) + b;

   return val 

########################################################################

def go(		nevents	= 	10000 				,\
			nbins	= 	100					,\
			b		= 	1					):

  npar				=	4
  mmin				=	0 
  mmax				=	2
  
  A 				= 	3
  x0 				= 	1
  sigma 			=	0.1

# Create a 1-Dim histogram with fix bins of type float.

  h1 = TH1F( 			"h1" 				,\
						"" 					,\
						nbins 				,\
						mmin				,\
						mmax				);
  
  h1.SetXTitle( 		"x"					);
  h1.SetYTitle( 		"nb. of events" 	);   

  gStyle.SetOptFit(		111111				);

# A TF1 object is a 1-Dim function defined between a lower and upper limit.

  ff1 = TF1(			"ff1"				,\
						f1					,\
						mmin				,\
						mmax				,\
						npar				)


  ff1.SetParameter( 	0, 		A		  	)
  ff1.SetParameter( 	1, 		x0		  	)
  ff1.SetParameter( 	2, 		sigma	  	)
  ff1.SetParameter( 	3, 		b		  	)

  ff1.SetMinimum(		0					)

  h1.FillRandom(		"ff1"				,\
						int((A+2*b)*nevents ))

# Create a new canvas with a predefined size form.

  c1 = TCanvas(			"c1"				,\
						"" 					,\
						1000				,\
						400					)

  ff1.SetParameter( 	0, 		A 	 * 0.7 	)
  ff1.SetParameter( 	1, 		x0 	 * 1.1 	)
  ff1.SetParameter( 	2, 		sigma* 1.5 	)
  ff1.SetParameter( 	3, 		0 		   	)  

  h1.SetMinimum(		0					)

  ff1.FixParameter(		0					,\
						500					)

  h1.Fit(				"ff1"				)

  ff1.ReleaseParameter(	0					)  

  h1.Fit(				"ff1"				)  

  print('Espected number of signal events: ', h1.GetBinWidth(1)*A*nevents)

########################################################################

  bgd	=	np.array(	[	0.0				,\
							0.5				,\
							1.0				,\
							2.0				,\
							5.0				,\
							10				,\
							30				,\
							50				])

  bgd	=	array(			"d"				,\
							bgd				)

  errA 	= 	np.array(	[	3.49543e+00		,\
							3.71073e+00		,\
							3.91263e+00 	,\
							4.28512e+00		,\
							5.20164e+00 	,\
							6.46264e+00		,\
							1.01335e+01		,\
							1.26267e+01		])
  
  errA	=	array(			"d"				,\
							errA			) 

# Create a new canvas with a predefined size form.

  c2 	= 	TCanvas(		"c2"			,\
							""				,\
							1000			,\
							400				)  

# A TGraph is an object made of two arrays X and Y with npoints each. 

  gr1	=	TGraph(			len(bgd)		,\
							bgd				,\
							errA			)
							
  gr1.SetMarkerStyle(		20				)

  gr1.Draw(					"ap"			)

  input(					"Hit to return"	)

########################################################################

def main():

   go()


if __name__ == "__main__":

    main()
